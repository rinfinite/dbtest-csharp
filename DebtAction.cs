﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBTest
{
    class DebtAction
    {
        public string Select(DbCommand command)
        {
            command.CommandText = "select count(*) from Debts";
            long count = (long)command.ExecuteScalar();

            return count.ToString();
        }

        public void Insert(DbCommand cmd, string row)
        {
            cmd.CommandText = Config.INSERT_DEBTS;

            addParamCsv(cmd, row);

            int result = cmd.ExecuteNonQuery();
            if (result < 1) Console.WriteLine("exception | result: " + result);
        }

        private void addParamCsv(DbCommand cmd, string line)
        {
            var values = line.Split('\t');
            cmd.Parameters.Clear();

            for (int i = 0; i < values.Length; i++)
            {
                string value = values[i];

                if (new int[] { 0 }.Contains(i)) addParam(cmd, i.ToString(), long.Parse(value));
                else if (new int[] { 1, 3, 17, 19, 20, 21 }.Contains(i)) addParam(cmd, i.ToString(), value);
                else if (new int[] { 2, 4, 5, 8, 16 }.Contains(i)) addParam(cmd, i.ToString(), decimal.TryParse(value.Replace(".", ","), out decimal decVal)?decVal:0);
                else if (new int[] { 6, 7, 13, 14, 18 }.Contains(i)) addParam(cmd, i.ToString(), DateTime.TryParse(value, out DateTime dateTime) ? dateTime : DateTime.MinValue);
                else if (new int[] { 9, 10, 12, 15 }.Contains(i)) addParam(cmd, i.ToString(), int.TryParse(value, out int result) ? result : 0);
                else if (new int[] { 11 }.Contains(i)) addParam(cmd, i.ToString(), byte.Parse(value));
            }
        }

        private void addParam(DbCommand cmd, string pName, object pValue)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = pName;
            p.Value = (object)pValue;
            cmd.Parameters.Add(p);
        }
    }
}
