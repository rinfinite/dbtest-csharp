using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DBTest
{
    class Run
    {
        public static Barrier barrier;
        public static List<string> rows = new List<string>();

        static void Main(string[] args)
        {
            /*
             * 1 - database
             * 2 - thread count
             */
            Console.WriteLine("file loading started");
            LoadFile();
            Console.WriteLine("file loading stopped | rows: " + rows.Count);

            string[] testArgs = new string[] { "psql", "4" };

            string dbProvider = testArgs.ElementAtOrDefault(0);
            int threadCount = int.Parse(testArgs.ElementAtOrDefault(1));

            List<Thread> threads = new List<Thread>();
            barrier = new Barrier(threadCount);

            for (int i = 0; i < threadCount; i++)
            {
                int threadIndex = i;
                threads.Add(new Thread(() => new Loader(dbProvider, threadIndex, rows.Count() / threadCount).Test()));
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();

            threads.ForEach(t => t.Start());
            threads.ForEach(t => t.Join());

            sw.Stop();
            Console.WriteLine("processing_time: " + sw.Elapsed);

            Console.WriteLine("FINISHED");
            Console.ReadLine();
        }

        static void LoadFile()
        {
            using (var reader = new StreamReader(Config.FILE_PATH))
            {
                while (!reader.EndOfStream)
                {
                    rows.Add(reader.ReadLine());
                }
            }
        }
    }
}
