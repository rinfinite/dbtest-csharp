﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBTest
{  
    class Loader
    {
        // commit from Ali
        private DBHelper _dbHelper;
        private int fromRow;
        private int toRow;

        public Loader(string dbProvider, int threadIndex, int rowsPartCount)
        {
            _dbHelper = new DBHelper(dbProvider);
            fromRow = threadIndex * rowsPartCount;
            toRow = fromRow + rowsPartCount;

            Run.barrier.SignalAndWait();
        }

        public void Test()
        {  //test Ali
            _dbHelper.Open();

            for (int i = fromRow; i < toRow; i++)
            {

                _dbHelper.command.Transaction = _dbHelper._dbConnection.BeginTransaction();
                new DebtAction().Insert(_dbHelper.command, Run.rows[i]);
                _dbHelper.command.Transaction.Commit();
            }

            _dbHelper.Close();
        }

        ~Loader()
        {
            _dbHelper.Close();
        }
    }
}
